CFLAGS=-O3 -Wall -Werror --std=c11 -pedantic
src=$(wildcard *.c)
OBJS=$(src:%.c=%.o)

.PHONY: clean watch valgrind run

fire: $(OBJS) Makefile
	cc $(CFLAGS) $(OBJS) -o fire -lSDL2

clean:
	rm -f fire $(OBJS)

watch:
	while inotifywait -e MOVE_SELF *; do\
		clear; make;\
	done

valgrind: fire Makefile
	valgrind --suppressions=valgrind-filter\
                 --track-origins=yes\
                 --leak-check=full\
                 ./fire

run: fire Makefile
	./fire
