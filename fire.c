#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include <SDL2/SDL.h>

#define PIXEL_FORMAT SDL_PIXELFORMAT_RGB888

struct res {
        Sint32 w;
        Sint32 h;
};

Uint32 *create_palette(SDL_PixelFormat *fmt)
{
        Uint32 *palette;

        if (!(palette = malloc(37 * sizeof(Uint32))))
                exit(1);

        palette[0]  = SDL_MapRGB(fmt, 0x00, 0x00, 0x00);
        palette[1]  = SDL_MapRGB(fmt, 0x1f, 0x07, 0x07);
        palette[2]  = SDL_MapRGB(fmt, 0x2f, 0x0f, 0x07);
        palette[3]  = SDL_MapRGB(fmt, 0x47, 0x0f, 0x07);
        palette[4]  = SDL_MapRGB(fmt, 0x57, 0x17, 0x07);
        palette[5]  = SDL_MapRGB(fmt, 0x67, 0x1f, 0x07);
        palette[6]  = SDL_MapRGB(fmt, 0x07, 0x07, 0x07);
        palette[7]  = SDL_MapRGB(fmt, 0x77, 0x1f, 0x07);
        palette[8]  = SDL_MapRGB(fmt, 0x8f, 0x27, 0x07);
        palette[9]  = SDL_MapRGB(fmt, 0x9f, 0x2f, 0x07);
        palette[10] = SDL_MapRGB(fmt, 0xaf, 0x3f, 0x07);
        palette[11] = SDL_MapRGB(fmt, 0xbf, 0x47, 0x07);
        palette[12] = SDL_MapRGB(fmt, 0xc7, 0x47, 0x07);
        palette[13] = SDL_MapRGB(fmt, 0xdf, 0x4f, 0x07);
        palette[14] = SDL_MapRGB(fmt, 0xdf, 0x57, 0x07);
        palette[15] = SDL_MapRGB(fmt, 0xdf, 0x57, 0x07);
        palette[16] = SDL_MapRGB(fmt, 0xd7, 0x5f, 0x07);
        palette[17] = SDL_MapRGB(fmt, 0xd7, 0x67, 0x0f);
        palette[18] = SDL_MapRGB(fmt, 0xcf, 0x6f, 0x0f);
        palette[19] = SDL_MapRGB(fmt, 0xcf, 0x77, 0x0f);
        palette[20] = SDL_MapRGB(fmt, 0xcf, 0x7f, 0x0f);
        palette[21] = SDL_MapRGB(fmt, 0xcf, 0x87, 0x17);
        palette[22] = SDL_MapRGB(fmt, 0xc7, 0x87, 0x17);
        palette[23] = SDL_MapRGB(fmt, 0xc7, 0x8f, 0x17);
        palette[24] = SDL_MapRGB(fmt, 0xc7, 0x97, 0x1f);
        palette[25] = SDL_MapRGB(fmt, 0xbf, 0x9f, 0x1f);
        palette[26] = SDL_MapRGB(fmt, 0xbf, 0x9f, 0x1f);
        palette[27] = SDL_MapRGB(fmt, 0xbf, 0xa7, 0x27);
        palette[28] = SDL_MapRGB(fmt, 0xbf, 0xa7, 0x27);
        palette[29] = SDL_MapRGB(fmt, 0xbf, 0xaf, 0x2f);
        palette[30] = SDL_MapRGB(fmt, 0xb7, 0xaf, 0x2f);
        palette[31] = SDL_MapRGB(fmt, 0xb7, 0xb7, 0x2f);
        palette[32] = SDL_MapRGB(fmt, 0xb7, 0xb7, 0x37);
        palette[33] = SDL_MapRGB(fmt, 0xcf, 0xcf, 0x6f);
        palette[34] = SDL_MapRGB(fmt, 0xdf, 0xdf, 0x9f);
        palette[35] = SDL_MapRGB(fmt, 0xef, 0xef, 0xc7);
        palette[36] = SDL_MapRGB(fmt, 0xff, 0xff, 0xff);

        return palette;
}

void burn(unsigned char *buf, const struct res *res)
{
        Sint32 x, y;
        int r;
        size_t pos;
        unsigned char new;

        for (y=1; y<res->h; y++) {
                for (x=0; x<res->w; x++) {
                        r = rand();
                        pos = y * res->w + x;
                        new = buf[pos] - (r & 1);

                        if (new != 255)
                                buf[pos - res->w + (r & 3)] = new;
                }
        }
}

void sdl_err(char *when)
{
        fprintf(stderr, "\nERROR: %s\n%s\n", when, SDL_GetError());
        exit(1);
}

void draw(SDL_Renderer *r,
          const Uint32 *palette,
          SDL_Texture *t,
          const unsigned char *buf,
          const size_t bufsize)
{
        Uint32 *pixels;
        int pitch;
        size_t i;

        SDL_LockTexture(t, NULL, (void *)&pixels, &pitch);
        for (i=0; i<bufsize; i++)
                pixels[i] = palette[buf[i]];
        SDL_UnlockTexture(t);

        SDL_RenderCopy(r, t, NULL, NULL);
        SDL_RenderPresent(r);
}

int input(bool *resized, struct res *res)
{
        SDL_Event event;

        while (SDL_PollEvent(&event)) {
                switch (event.type) {
                case SDL_QUIT:
                        return 0;
                case SDL_KEYDOWN:
                        switch (event.key.keysym.sym) {
                        case SDLK_ESCAPE:
                                return 0;
                        }
                        continue;
                case SDL_WINDOWEVENT:
                        if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                                *resized = 1;
                                res->w = event.window.data1;
                                res->h = event.window.data2;
                        }
                        continue;
                }
        }
        return 1;
}

SDL_Texture *texture(SDL_Renderer *r, const struct res *res, SDL_Texture *t)
{
        if (t)
                SDL_DestroyTexture(t);

        if (!(t = SDL_CreateTexture(r,
                                    PIXEL_FORMAT,
                                    SDL_TEXTUREACCESS_STREAMING,
                                    res->w, res->h)))
                sdl_err("Creating Texture");

        return t;
}

unsigned char *init(unsigned char *buf, const struct res *res)
{
        const size_t size = res->w * res->h * sizeof(char);
        unsigned char *new;

        if (!(new = realloc(buf, size))) {
                free(buf);
                exit(1);
        }

        memset(new, 36, size);
        return new;
}

unsigned lap(void)
{
        static struct timeval then;
        struct timeval now;
        unsigned ms;

        if (gettimeofday(&now, NULL) == -1)
                abort();

        if (!then.tv_sec)
                then = now;

        ms = now.tv_sec - then.tv_sec;
        ms *= 1000000;
        ms += now.tv_usec - then.tv_usec;

        then = now;

        return ms;
}

int main(void)
{
        SDL_Window *w;
        SDL_Renderer *r;
        SDL_PixelFormat *fmt;
        SDL_Texture *t = NULL;

        Uint32 *palette;
        unsigned char *buf = NULL;

        struct res res = { .w = 640, .h = 480 };
        bool resized = 0;

        int ms = 0;
        int frames = 0;

        srand(time(NULL));

        if (SDL_Init(SDL_INIT_VIDEO))
                sdl_err("Init");

        if (!(w = SDL_CreateWindow(
                        "I AM THE GOD OF HELLFIRE, AND I BRINsegfault",
                        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                        res.w, res.h,
                        SDL_WINDOW_RESIZABLE)))
                sdl_err("Creating Window");

        if (!(r = SDL_CreateRenderer(
                        w, -1,
                        SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC)))
                sdl_err("Creating Renderer");

        fmt = SDL_AllocFormat(PIXEL_FORMAT);

        palette = create_palette(fmt);
        t = texture(r, &res, t);
        buf = init(buf, &res);
        while (input(&resized, &res)) {
                if (resized) {
                        t = texture(r, &res, t);
                        buf = init(buf, &res);
                        resized = 0;
                }

                burn(buf, &res);
                draw(r, palette, t, buf, res.w*res.h);

                if ((ms += lap()) > 1000000) {
                        printf("fps: %d\n", frames);
                        frames = ms = 0;
                } else {
                        frames++;
                }
        }

        SDL_DestroyTexture(t);
        SDL_DestroyRenderer(r);
        SDL_DestroyWindow(w);
        SDL_Quit();
        free(buf);
        free(palette);

        return 0;
}
